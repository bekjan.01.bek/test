from django.urls import path, include
from rest_framework import routers
from task.views import TaskViewSet

router = routers.DefaultRouter()
router.register(r'task', TaskViewSet)

urlpatterns = [
    path('api/v1/', include(router.urls)),
]